
import 'package:flutter/material.dart';
import 'package:to_do_list/new_item_view.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(),
    );
  }
}
 class MyHomePage extends StatefulWidget{

  @override
   _MyHomePageState createState() =>  _MyHomePageState();
   
    }
   
   class _MyHomePageState extends State<MyHomePage> {

     List<ToDo> list = List<ToDo>();

     @override
     void initState(){
       list.add(ToDo(title: 'item A'));
       list.add(ToDo(title: 'item B'));
       list.add(ToDo(title: 'item C'));
       super.initState();

     }
      @override
      Widget build(BuildContext context) {
       return Scaffold(
         appBar: AppBar(
           title: Text('To Do List'),
           centerTitle: true,
         ),
         //ListView.Builder
        //  body: ListView.builder(
        //    itemCount: list.length,
        //    itemBuilder: (context, index) {
        //      return ListTile(
        //       title: Text(list[index].title),
        //       trailing: Checkbox(value: list[index].complete , onChanged: null),
        //        onTap: () => setCompleteness(list[index]),
        //      );
        //    },
        //  ),
         //for ListView
        //  body: ListView(
        //   children: <Widget>[
        //     ListTile(
        //       title: Text('Item 1'),
        //       trailing: Icon(Icons.check_box),
        //       onTap: () => print('Sat'),
        //     ),
        //       ListTile(
        //       title: Text('Item 2'),
        //       trailing: Icon(Icons.check_box),
        //       onTap: () => print('Sun'),
        //     ),
        //       ListTile(
        //       title: Text('Item 3'),
        //       trailing: Icon(Icons.check_box),
        //       onTap: () => print('Mon'),
        //     ),
        //       ListTile(
        //       title: Text('Item 4'),
        //       trailing: Icon(Icons.check_box),
        //       onTap: () => print('Tue'),
        //     ),
        //       ListTile(
        //       title: Text('Item 5'),
        //       trailing: Icon(Icons.check_box),
        //       onTap: () => print('Wed'),
        //     ),
        //       ListTile(
        //       title: Text('Item 6'),
        //       trailing: Icon(Icons.check_box),
        //       onTap: () => print('Thu'),
        //     ),
        //       ListTile(
        //       title: Text('Item 7'),
        //       trailing: Icon(Icons.check_box),
        //       onTap: () => print('Fri'),
        //     ),
        //   ],
        //  )

        body: list.isNotEmpty ? buildBody():buildEmptyBody(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: ()=> gotoNewItemView(),
        ),

       );
    
     }
        Widget buildBody(){
         return ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
             return buildItem(list[index]);
            },
         );
       }
       Widget buildEmptyBody(){
         return Center(
           child: Text('No Item yet!!!')
         );
       }

       Widget buildItem(ToDo item){
          return Dismissible(
            key: Key(item.hashCode.toString()),
            onDismissed: (direction) => removeItem(item),
            direction: DismissDirection.startToEnd,
            background: Container(
              color: Colors.redAccent,
              child: Icon(Icons.delete , color: Colors.white),
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 12.0),
            ),
            child: buildEditTile(item),
          );

       }

       Widget buildEditTile(ToDo item){
         return ListTile(
              title: Text(item.title),
               trailing: Checkbox(value: item.complete , onChanged: null),
               onTap: () => setCompleteness(item),
               onLongPress: () => goToEditItemView(item),
              );
       }


       void gotoNewItemView(){
         Navigator.of(context).push(MaterialPageRoute(
           builder: (context){
             return NewItemView();
           }
         
         )).then((title){
           if(title != null) addToDo(ToDo(title: title));
        
         });
       }
       
       void goToEditItemView(ToDo item){
          Navigator.of(context).push(MaterialPageRoute(
           builder: (context){
             return NewItemView(title: item.title);
           }
         
         )).then((title){
           if(title != null) editToDo(item,title);
        
         });
         
       }
   
     void setCompleteness(ToDo item){
      setState((){
        item.complete = !item.complete;
        });

    }
        void removeItem(ToDo item){
         list.remove(item);
         if(list.isEmpty) setState(() {
           
         });
       }

       void addToDo(ToDo item){
         list.add(item);

       }

       void editToDo(ToDo item, String title){
         item.title = title;
       }
}



class ToDo{
  String title;
  bool complete;
  ToDo({
    this.title,
    this.complete = false,
  });
}

